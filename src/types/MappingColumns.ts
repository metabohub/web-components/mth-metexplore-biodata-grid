export interface MappingColumns {
  // key: name of the mapping
  [key: string]: {
    // on which column to map, by default: 'db_identifier'
    onColumn?: string;
    // list of identifiers mapped
    listIds: string[];
    // list of enrichment columns
    stats?: Array<{ id: string, pval: number, bonferroni_pval: number, bh_pval: number }>;
  };
}
