// matches the API paths
export enum Biodata {
  PATHWAY = "pathways",
  REACTION = "reactions",
  METABOLITE = "metabolites",
  ENZYME = "enzymes",
  GENE = "genes",
  PROTEIN = "proteins",
  NETWORK = "networks",
}
