import type { MthGridItem } from "@metabohub/mth-grid";
import { Biodata } from "@/types/Biodata";
import {
  Configuration,
  NetworksApi,
  PathwaysApi,
  ReactionsApi,
  MetabolitesApi,
  EnzymesApi,
  GenesApi,
  ProteinsApi,
} from "@metexplore/metexplore3-nodejs-api/src/lib/generated-api-lib";
import type { AxiosResponse } from "axios";

export function useApiService(backendPath: string) {
  const apiConfiguration = new Configuration({ basePath: backendPath });

  async function list(
    biodata: Biodata,
    idNetwork?: number,
  ): Promise<MthGridItem[]> {
    try {
      let response: AxiosResponse<any>;
      switch (biodata) {
        case Biodata.NETWORK:
          response = await new NetworksApi(apiConfiguration).gETNETWORKS();
          return response.data.networks as MthGridItem[];
        case Biodata.PATHWAY:
          response = await new PathwaysApi(apiConfiguration).gETPATHWAYS(
            undefined,
            idNetwork,
          );
          break;
        case Biodata.REACTION:
          response = await new ReactionsApi(apiConfiguration).gETREACTIONS(
            idNetwork,
          );
          break;
        case Biodata.METABOLITE:
          response = await new MetabolitesApi(apiConfiguration).gETMETABOLITES(
            undefined,
            idNetwork,
          );
          return response.data.results as MthGridItem[];
        case Biodata.ENZYME:
          response = await new EnzymesApi(apiConfiguration).gETENZYMES(
            idNetwork,
          );
          break;
        case Biodata.GENE:
          response = await new GenesApi(apiConfiguration).gETGENES(idNetwork);
          break;
        default: // Biodata.PROTEIN
          response = await new ProteinsApi(apiConfiguration).gETPROTEINS(
            undefined,
            idNetwork,
          );
          break;
      }

      return response.data.results;
    } catch {
      return [];
    }
  }

  return { list };
}
