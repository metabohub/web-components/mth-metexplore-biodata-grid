import { beforeEach, describe, expect, test, vi } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import vuetify from "@/plugins/vuetify";
import MthBiodataGrid from "@/components/MthBiodataGrid.vue";
import type { MthGridItem, SelectStrategy } from "@metabohub/mth-grid";
import { Biodata } from "@/types/Biodata";
import type { MappingColumns } from "@/types/MappingColumns";

// Mock the ResizeObserver
const ResizeObserverMock = vi.fn(() => ({
  observe: vi.fn(),
  unobserve: vi.fn(),
  disconnect: vi.fn(),
}));

// Stub the global ResizeObserver
vi.stubGlobal("ResizeObserver", ResizeObserverMock);

// TODO : test local mode
function mthBiodataGridwrapper(
  network: number,
  biodata: Biodata,
  filterOnIds: Array<string> | null,
  slot: string | null,
  mapping?: MappingColumns,
  numberMappingPreSelected?: number,
  selectStrategy?: SelectStrategy,
) {
  return slot === null
    ? mount(MthBiodataGrid, {
        global: {
          plugins: [vuetify],
        },
        props: {
          mode: "remote",
          backendPath: "",
          network,
          biodata,
          filterOnIds,
          mapping,
          numberMappingPreSelected,
          selectStrategy,
        },
      })
    : mount(MthBiodataGrid, {
        global: {
          plugins: [vuetify],
        },
        props: {
          mode: "remote",
          backendPath: "",
          network,
          biodata,
          filterOnIds,
          mapping,
          numberMappingPreSelected,
          selectStrategy,
        },
        slots: {
          gridActions: slot,
        },
      });
}

// mock call to API
const mockApiServices = {
  list: vi.fn(),
};
vi.mock("@/composables/ApiServices", () => ({
  useApiService: () => mockApiServices,
}));

// DATA
const items: Array<MthGridItem> = [
  {
    id: 932417,
    name: "	Glutamate metabolism",
    db_identifier: "Glutamate_metabolism",
  },
  {
    id: 932434,
    name: "NAD metabolism",
    db_identifier: "NAD_metabolism",
  },
  {
    id: 932502,
    name: "Vitamin B12 metabolism",
    db_identifier: "Vitamin_B12_metabolism",
  },
];

describe("MthBiodataGrid.vue", () => {
  // return fake values
  beforeEach(() => {
    mockApiServices.list.mockResolvedValue(items);
  });
  test("should display the content of the slot", async () => {
    const slot = "<template #gridActions><div class='mySlot' /></template>";
    const wrapper = mthBiodataGridwrapper(4649, Biodata.ENZYME, null, slot);
    // await call to API
    await flushPromises();

    // Slot for each row
    expect(wrapper.findAll(".mySlot").length).equals(items.length);
  });
  test("should display only the filtered rows", async () => {
    const listIds = ["Glutamate_metabolism"]; // DB ID : Glutamate metabolism
    const wrapper = mthBiodataGridwrapper(4649, Biodata.ENZYME, listIds, null);
    // await call to API
    await flushPromises();

    // expect table to only show filtered rows
    expect(wrapper.findAll("tbody tr").length).equals(listIds.length);
    // DB ID value
    expect(wrapper.findAll("tbody td")[1].element.textContent).toEqual(
      listIds[0],
    );
  });

  test("should display the mapped columns", async () => {
    const mapping: MappingColumns = {
      mapping: { listIds: ["Glutamate_metabolism"] },
    };
    const wrapper = mthBiodataGridwrapper(
      4649,
      Biodata.ENZYME,
      null,
      null,
      mapping,
    );
    // await call to API
    await flushPromises();

    // expect table to display the mapped columns
    expect(wrapper.findAll("thead th").at(3).text()).toEqual("mapping");
    expect(wrapper.findAll("tbody tr").at(0).findAll("td").at(3).text()).toEqual("true");
    expect(wrapper.findAll("tbody tr").at(1).findAll("td").at(3).text()).toEqual("false");
    expect(wrapper.findAll("tbody tr").at(2).findAll("td").at(3).text()).toEqual("false");
  });

  test("should display the enrichement stats", async () => {
    const mapping: MappingColumns = {
      mapping: {
        listIds: ["Glutamate_metabolism"],
        stats: [
          {
            id: "Glutamate_metabolism",
            pval: 0.05,
            bonferroni_pval: 0.05,
            bh_pval: 0.05,
          },
        ],
      },
    };
    const wrapper = mthBiodataGridwrapper(
      4649,
      Biodata.ENZYME,
      null,
      null,
      mapping,
    );
    // await call to API
    await flushPromises();

    // expect table to display the enrichement stats
    expect(wrapper.findAll("thead th").at(3).text()).toEqual("mapping");
    expect(wrapper.findAll("thead th").at(4).text()).toEqual("P value");
    expect(wrapper.findAll("thead th").at(5).text()).toEqual("Bonferroni p value");
    expect(wrapper.findAll("thead th").at(6).text()).toEqual("Benjamini Hochsberg p value");
    expect(wrapper.findAll("tbody tr").at(0).findAll("td").at(4).text()).toEqual("0.05");
    expect(wrapper.findAll("tbody tr").at(0).findAll("td").at(5).text()).toEqual("0.05");
    expect(wrapper.findAll("tbody tr").at(0).findAll("td").at(6).text()).toEqual("0.05");
    expect(wrapper.findAll("tbody tr").at(1).findAll("td").at(4).text()).toEqual("");
    expect(wrapper.findAll("tbody tr").at(1).findAll("td").at(5).text()).toEqual("");
    expect(wrapper.findAll("tbody tr").at(1).findAll("td").at(6).text()).toEqual("");
    expect(wrapper.findAll("tbody tr").at(2).findAll("td").at(4).text()).toEqual("");
    expect(wrapper.findAll("tbody tr").at(2).findAll("td").at(5).text()).toEqual("");
    expect(wrapper.findAll("tbody tr").at(2).findAll("td").at(6).text()).toEqual("");
  });
});
