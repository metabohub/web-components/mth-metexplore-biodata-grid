import { beforeEach, describe, expect, test, vi } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import vuetify from "@/plugins/vuetify";
import MthNetworkGrid from "@/components/MthNetworkGrid.vue";
import type { Network } from "@metexplore/metexplore3-nodejs-api/src/lib/generated-api-lib";
import type { SelectStrategy } from "@metabohub/mth-grid";

// Mock the ResizeObserver
const ResizeObserverMock = vi.fn(() => ({
  observe: vi.fn(),
  unobserve: vi.fn(),
  disconnect: vi.fn(),
}));

// Stub the global ResizeObserver
vi.stubGlobal("ResizeObserver", ResizeObserverMock);

// TODO : test local mode
function networkGridwrapper(
  filterBy: "PRIVATE" | "PUBLIC" | "TOP" | null,
  slot: string | null,
  selectStrategy?: SelectStrategy,
) {
  return slot === null
    ? mount(MthNetworkGrid, {
        global: {
          plugins: [vuetify],
        },
        props: {
          mode: "remote",
          backendPath: "",
          filterBy,
          selectStrategy,
        },
      })
    : mount(MthNetworkGrid, {
        global: {
          plugins: [vuetify],
        },
        props: {
          mode: "remote",
          backendPath: "",
          filterBy,
          selectStrategy,
        },
        slots: {
          gridActions: slot,
        },
      });
}

// mock call to API
const mockApiServices = {
  list: vi.fn(),
};
vi.mock("@/composables/ApiServices", () => ({
  useApiService: () => mockApiServices,
}));

// DATA
const items: Network[] = [
  {
    id: 1,
    db_identifier: "1757",
    comments: "BioCyc",
    name: "Acinetobacter baumannii - 6013113 - Ab6013113",
    //top: false,
    //private: true,
  },
  {
    id: 2,
    db_identifier: "4311",
    comments: "Others (SBML, ...)",
    name: "Homo sapiens - Global - Swainston2016 - Reconstruction of human metabolic network (Recon 2.2)",
    //top: true,
    //private: false,
  },
  {
    id: 3,
    db_identifier: "1803",
    comments: "BioCyc",
    name: "Escherichia coli - 042 - EC42131",
    //top: false,
    //private: false,
  },
];

describe("NetworkGrid.vue", () => {
  // return fake values
  beforeEach(() => {
    mockApiServices.list.mockResolvedValue(items);
  });
  test("should emit event 'selection' when a row is selected", async () => {
    const filterBy = null;
    const wrapper = networkGridwrapper(filterBy, null);
    // await call to API
    await flushPromises();

    // select a checkbox
    const checkbox = wrapper.findAll("input[type='checkbox']")[0];
    expect(wrapper.find(".mdi-checkbox-marked").exists()).toBe(false);
    await checkbox.trigger("click");
    expect(wrapper.find(".mdi-checkbox-marked").exists()).toBe(true);
    // expect 'selection' to have been emitted
    expect(wrapper.emitted().selection).toBeDefined();
    // emitted data should be the line that was checked
    expect((wrapper.emitted().selection[0] as Array<Network>)[0]).toEqual([
      items[0],
    ]);
  });
  /*
  test("should only show public networks when filter on 'public'", async () => {
    const filterBy = "PUBLIC";
    const wrapper = networkGridwrapper(filterBy, null);
    // await call to API
    await flushPromises();

    // there should be rows 4311 and 1803 but not 1757
    const rows = wrapper.findAll("td").map((td) => {
      return td.element.textContent;
    });
    expect(rows.includes("4311")).toBe(true);
    expect(rows.includes("1803")).toBe(true);
    expect(rows.includes("1757")).toBe(false);
  });
  test("should only show private networks when filter on 'private'", async () => {
    const filterBy = "PRIVATE";
    const wrapper = networkGridwrapper(filterBy, null);
    // await call to API
    await flushPromises();

    // there should be row 1757 but not 4311 and 1803
    const rows = wrapper.findAll("td").map((td) => {
      return td.element.textContent;
    });
    expect(rows.includes("1757")).toBe(true);
    expect(rows.includes("4311")).toBe(false);
    expect(rows.includes("1803")).toBe(false);
  });
  test("should only show top networks when filter on 'top'", async () => {
    const filterBy = "TOP";
    const wrapper = networkGridwrapper(filterBy, null);
    // await call to API
    await flushPromises();

    // there should be row 4311 but not 1757 and 1803
    const rows = wrapper.findAll("td").map((td) => {
      return td.element.textContent;
    });
    expect(rows.includes("4311")).toBe(true);
    expect(rows.includes("1757")).toBe(false);
    expect(rows.includes("1803")).toBe(false);
  });
  */
  test("should display the content of the slot", async () => {
    const filterBy = null;
    const slot = "<template #gridActions><div class='mySlot' /></template>";
    const wrapper = networkGridwrapper(filterBy, slot);
    // await call to API
    await flushPromises();

    // Slot for each row
    expect(wrapper.findAll(".mySlot").length).equals(items.length);
  });
});
