<template>
  <MthGrid
    :headers
    :items="trueItems"
    :is-loading="false"
    :select-strategy
    item-select-key="id"
    :dark
    @selection="emit('selection', $event)"
  >
    <template
      v-if="displayActions"
      #actions="{ itemAction }"
    >
      <slot
        name="gridActions"
        :item-action="itemAction"
      />
    </template>
  </MthGrid>
</template>

<script setup lang="ts">
import { ref, computed, useSlots, onMounted } from "vue";
import { useApiService } from "@/composables/ApiServices";
import { Biodata } from "@/types/Biodata";
import type { MappingColumns } from "@/types/MappingColumns";
import { MthGrid } from "@metabohub/mth-grid";
import "@metabohub/mth-grid/dist/style.css";
import type {
  MthGridHeader,
  MthGridItem,
  SelectStrategy,
} from "@metabohub/mth-grid";

// PROPS
export interface Props {
  mode: "local"|"remote";
  // if mode is remote, we need the backend path and the network to call the API
  backendPath?: string;
  network?: number;
  // else if mode is local, we need the biodata to display
  data?: MthGridItem[];
  // for the headers and the API call
  biodata: Biodata;
  // list of ids (db identifier) to filter on, null if no filter
  filterOnIds?: string[] | null;
  // name of the mapping with a list of the 'db_identifier' that are mapped
  mapping?: MappingColumns;
  // maximum number of mapping columns to display
  numberMappingPreSelected?: number;
  selectStrategy?: SelectStrategy;
  dark?: boolean;
}
const props = withDefaults(defineProps<Props>(), {
  backendPath: undefined,
  network: undefined,
  data: undefined,
  filterOnIds: null,
  mapping: undefined,
  numberMappingPreSelected: 3,
  selectStrategy: "all",
  dark: false,
});

// EMITS
const emit = defineEmits(["selection", "error"]);

// DATA
const slots = useSlots();
const displayActions = ref<boolean>("gridActions" in slots);
const items = ref<MthGridItem[]>();

// COMPUTED
const headers = computed((): MthGridHeader[] => {
  const tmp: MthGridHeader[] = [
    { title: "Id", key: "db_identifier", sortable: true, searchable: true, width: 100 },
    {
      title: "Name",
      key: "name",
      sortable: true,
      searchable: true,
      vHtml: true,
      width: 250,
    },
  ];
  if (props.biodata == Biodata.REACTION) {
    tmp.push({
      title: "EC",
      key: "ec",
      sortable: true,
      searchable: true,
      width: 100,
    });
    tmp.push({
      title: "Equation",
      key: "eq_name",
      sortable: true,
      searchable: true,
      vHtml: true,
      width: 300,
    });
    tmp.push({
      title: "Reversible",
      key: "reversible",
      selected: false,
      sortable: true,
      width: 100,
    })
  } else if (props.biodata == Biodata.METABOLITE) {
    tmp.push({
      title: "Chemical Formula",
      key: "chemical_formula",
      sortable: true,
      searchable: true,
      vHtml: true,
      width: 100,
    });
    tmp.push({
      title: "Weight",
      key: "weight",
      sortable: true,
      width: 100,
    });
    tmp.push({
      title: "Average mass",
      key: "average_mass",
      selected: false,
      sortable: true,
      width: 100,
    });
    tmp.push({
      title: "Charge",
      key: "charge",
      selected: false,
      sortable: true,
      width: 100,
    });
    tmp.push({
      title: "Generic",
      key: "generic",
      selected: false,
      sortable: true,
      width: 100,
    });
    tmp.push({
      title: "INCHI",
      key: "inchi",
      selected: false,
      sortable: true,
      width: 100,
    });
    tmp.push({
      title: "SMILES",
      key: "smiles",
      selected: false,
      sortable: true,
      width: 100,
    });
    
  } else if (props.biodata == Biodata.PATHWAY) {
    tmp.push({
      title: "Size (nb of reactions)",
      key: "nb_reactions",
      sortable: true,
      width: 100,
    });
  }
  // mapping columns
  if (props.mapping != undefined) {
    Object.keys(props.mapping).forEach((key, i) => {
      // add the header only if the key is available
      if (!tmp.find((h) => h.key == key)) {
        // add a chip column
        tmp.push({
          title: key,
          key: key,
          sortable: true,
          width: 100,
          chip: true,
          chipColors: { true: "green", false: "red" },
          // only display the first mappings
          selected: i >= props.numberMappingPreSelected ? false : true,
        });
        if (props.mapping![key].stats != undefined) {
          // add enrichment columns
          tmp.push({ title: "P value", key: "pval", sortable: true, width: 100 });
          tmp.push({ title: "Bonferroni p value", key: "bonferroni_pval", sortable: true, width: 100 });
          tmp.push({ title: "Benjamini Hochsberg p value", key: "bh_pval", sortable: true, width: 100 });
        }
      } else {
        console.warn("[BiodataGrid] The header '" + key + "' already exists");
      }
    });
  }

  return tmp;
});

const trueItems = computed((): MthGridItem[] => {
  // no data yet
  if (props.mode == "remote" && (items.value == undefined)) {
    return [];
  }

  let tmp: MthGridItem[] = [];

  // props.data must be in the condition or else 
  // its change wont be noticed by the computed
  if (props.mode == "local" && props.data != undefined) {
    tmp = JSON.parse(
      JSON.stringify(
        filterOnData(
          props.data as MthGridItem[], 
          props.filterOnIds),
      ),
    );
  } else {
    tmp = JSON.parse(
      JSON.stringify(
        filterOnData(
          items.value as MthGridItem[], 
          props.filterOnIds),
      ),
    );
  }
  
  tmp.forEach((item) => {
    // format the name
    item.name = (item.name as string).replaceAll("less_than", "<");
    item.name = (item.name as string).replaceAll("greater_than", ">");
    // if equation : format the equation
    if (item.eq_name) {
      item.eq_name = item.eq_name.replaceAll("less_than", "<");
      item.eq_name = item.eq_name.replaceAll("greater_than", ">");
    }
    // if chemical formula : format the formula
    if (item.chemical_formula) {
      item.chemical_formula = item.chemical_formula.replaceAll("less_than", "<");
      item.chemical_formula = item.chemical_formula.replaceAll("greater_than", ">");
    }

    // add the mapped values
    if (props.mapping != undefined) {
      Object.entries(props.mapping as MappingColumns).forEach(([key, value]) => {
        // if no column name specified, map on 'db_identifier'
        const mappingColumn =
          (value as { onColumn: string, listIds: string[] }).onColumn == undefined ? "db_identifier" : (value as { onColumn: string, listIds: string[] }).onColumn;
        // add mapping on the choosen column
        if ((value as { onColumn: string, listIds: string[] }).listIds.includes(item[mappingColumn] as string)) {
          item[key] = "true";
        } else {
          item[key] = "false";
        }
        if (value.stats != undefined) {
          // the items where to add the stats
          const matches = value.stats.map(s => s.id);
          if (matches.includes(item.db_identifier as string)) {
            item.pval = value.stats.find(i => i.id == item.db_identifier)!.pval;
            item.bonferroni_pval = value.stats.find(i => i.id == item.db_identifier)!.bonferroni_pval;
            item.bh_pval = value.stats.find(i => i.id == item.db_identifier)!.bh_pval;
          }
        }
      });
    }
  });

  return tmp;
});

// HOOKS
onMounted(async () => {
  if (props.mode == "local") {
    // check the props
    if (props.data == undefined) {
      emit("error", {
        title: "Error while displaying the data",
        message: "The data was not provided",
      });
    }
  } else if (props.mode == "remote") {
    // check the props
    if (props.backendPath == undefined || props.network == undefined) {
      emit("error", {
        title: "Error while fetching the data",
        message: "The backend path or the network were not provided",
      });
    } else {
      // fetch data from the API
      try {
        items.value = await useApiService(props.backendPath as string).list(props.biodata, props.network);
      } catch (e) {
        emit("error", {
          title: "Error while fetching the data",
          message: e.toString(),
        });
      }
    }
  }
});

// METHODS
// filter the data array with the ids (db_identifier) in filters array
// function instead of const to make it available for items
function filterOnData(
  data: Array<MthGridItem>,
  filters: Array<string> | null,
): MthGridItem[] {
  if (filters === null) {
    return data as MthGridItem[];
  } else {
    return data.filter((e) =>
      filters.includes(e.db_identifier as string),
    ) as MthGridItem[];
  }
};
</script>
