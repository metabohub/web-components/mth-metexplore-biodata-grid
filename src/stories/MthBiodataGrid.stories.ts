import { MthGridItem } from '@metabohub/mth-grid';
import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import MthBiodataGrid from "../components/MthBiodataGrid.vue";
import { Biodata } from "@/types/Biodata";
import type { MappingColumns } from "@/types/MappingColumns";

const meta: Meta<typeof MthBiodataGrid> = {
  title: "MthBiodataGrid",
  component: MthBiodataGrid,
  tags: ["autodocs"],
  argTypes: {
    mode: {
      control: "select",
      options: ["local", "remote"]
    },
    biodata: {
      options: [
        Biodata.PATHWAY,
        Biodata.REACTION,
        Biodata.METABOLITE,
        Biodata.ENZYME,
        Biodata.GENE,
        Biodata.PROTEIN,
      ],
      control: {
        type: "select",
      },
    },
  },
};

export default meta;
type Story = StoryObj<typeof MthBiodataGrid>;

const network: number = 207;
const biodata: Biodata = Biodata.METABOLITE;
const filterOnIds: Array<string> | null = null;
const data: MthGridItem[] = [
  {
    id: 1,
    db_identifier: "TRIPEPTIDES",
    name: "a tripeptide",
    weight: 0,
    chemical_formula: "",
  },
  {
    id: 2,
    db_identifier: "PD00364",
    name: "MarR transcriptional repressor",
    weight: 0,
    chemical_formula: "",
  },
  {
    id: 3,
    db_identifier: "LL-DIAMINOPIMELATE",
    name: "L,L-diaminopimelate",
    weight: 190.09536,
    chemical_formula: "C7H14N2O4",
  },
];
const mapping: MappingColumns = {
  mapping: { listIds: ["TRIPEPTIDES", "PD00364"] },
  by_weight: {
    onColumn: "weight",
    listIds: ["0.0"],
  },
};
const mappingWithEnrichment: MappingColumns = {
  mapping: { listIds: ["TRIPEPTIDES", "PD00364"], stats: [
    { id: "TRIPEPTIDES", pval: 0.01, bonferroni_pval: 0.08, bh_pval: 0.02 },
    { id: "PD00364", pval: 0.05, bonferroni_pval: 0.06, bh_pval: 0.1 },
  ] },
};

export const Default: Story = {
  render: (args) => ({
    components: { MthBiodataGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthBiodataGrid :mode :data :biodata :filterOnIds @selection='selection' />",
  }),
  args: {
    mode: "local",
    data,
    biodata,
    filterOnIds,
  },
};

export const WithSlot: Story = {
  render: (args) => ({
    components: { MthBiodataGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthBiodataGrid :mode :data :biodata :filterOnIds @selection='selection' >" +
      "<template #gridActions><v-btn>info</v-btn></template>" +
      "</MthBiodataGrid>",
  }),
  args: {
    mode: "local",
    data,
    biodata,
    filterOnIds,
  },
};

export const Mapping: Story = {
  render: (args) => ({
    components: { MthBiodataGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthBiodataGrid :mode :data :biodata :filterOnIds :mapping @selection='selection' />",
  }),
  args: {
    mode: "local",
    data,
    biodata,
    filterOnIds,
    mapping,
  },
};

export const MappingWithEnrichment: Story = {
  render: (args) => ({
    components: { MthBiodataGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthBiodataGrid :mode :data :biodata :filterOnIds :mapping @selection='selection' />",
  }),
  args: {
    mode: "local",
    data,
    biodata,
    filterOnIds,
    mapping: mappingWithEnrichment,
  },
};
