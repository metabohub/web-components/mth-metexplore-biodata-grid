import { MthGridItem } from '@metabohub/mth-grid';
import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import MthNetworkGrid from "@/components/MthNetworkGrid.vue";

const meta: Meta<typeof MthNetworkGrid> = {
  title: "MthNetworkGrid",
  component: MthNetworkGrid,
  tags: ["autodocs"],
  argTypes: {
    mode: {
      control: "select",
      options: ["local", "remote"]
    },
    filterBy: {
      control: "select",
      options: ["PRIVATE", "PUBLIC", "TOP", null],
    },
    selectStrategy: {
      control: "select",
      options: ["all", "pages", "single"],
    },
  },
};

export default meta;
type Story = StoryObj<typeof MthNetworkGrid>;

const data: MthGridItem[] = [
  {
    "organism_name": "Not defined",
    "id": 207,
    "name": "ECOLI",
    "db_identifier": "ECOLI",
  },
  {
    "organism_name": "Not defined",
    "id": 209,
    "name": "Ec_iAF1260",
    "db_identifier": "_151a36b8_6e72_4a95_966a_dfdb46cffd18",
  },
  {
    "organism_name": "Not defined",
    "id": 210,
    "name": "TEST",
    "db_identifier": "TEST",
  }
];

export const Default: Story = {
  render: (args) => ({
    components: { MthNetworkGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthNetworkGrid :mode :data :filterBy :selectStrategy @selection='selection' />",
  }),
  args: {
    mode: "local",
    data,
    filterBy: null,
    selectStrategy: "all",
  },
};

export const WithSlot: Story = {
  render: (args) => ({
    components: { MthNetworkGrid },
    setup() {
      return {
        ...args,
        selection: action("selection"),
      };
    },
    template:
      "<MthNetworkGrid :mode :data :filterBy :selectStrategy @selection='selection'>" +
      "<template #gridActions><v-btn>info</v-btn></template>" +
      "</MthNetworkGrid>",
  }),
  args: {
    mode: "local",
    data,
    filterBy: null,
    selectStrategy: "all",
  },
};
