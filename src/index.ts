import type { App } from "vue";
import { MthNetworkGrid, MthBiodataGrid } from "@/components";
import { Biodata } from "@/types/Biodata";
import type { MappingColumns } from "@/types/MappingColumns";

export default {
  install: (app: App) => {
    app.component("MthNetworkGrid", MthNetworkGrid);
    app.component("MthBiodataGrid", MthBiodataGrid);
  },
};

export {
  MthNetworkGrid,
  MthBiodataGrid,
  Biodata,
  MappingColumns,
};
