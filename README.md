# mth-metexplore-biodata-grid

`MthNetworkGrid` and `MthBiodataGrid` are Vue 3 component that display data in a grid with 2 modes to either fetch data from an API or provide it yourself.

## Types

| Name | Value |
| --- | --- |
| `Biodata` | `enum { PATHWAY, REACTION, METABOLITE, ENZYME, GENE, PROTEIN, NETWORK }` |
| `MappingColumns` | `{ [key: string]: { onColumn?: string; listIds: string[]; stats?: { id: string, pval: number, bonferroni_pval: number, bh_pval: number }[] } };` |

## MthNetworkGrid

### Props

| Name              | Description                                   | Type                                     | Default    |
| ----------------- | --------------------------------------------- | ---------------------------------------- | ---------- |
| `mode`            | remote : the data is fetched from the API with `backendPath`. local : the user provides the data with `data`. | `"local"\|"remote"`                                 | -          |
| `backendPath?`    | path to the api if mode is remote             | `string`                                 | -          |
| `data?`           | the data to display if mode is local          | `MthGridItem[]`                          | -          |
| `filterBy`        | By which properties the data must be filtered | `null \| "PRIVATE" \| "PUBLIC" \| "TOP"` | -          |
| `selectStrategy?` | the select type : single or multiple          | `"all" \| "page" \| "single"`            | `"single"` |

### Events

| Name        | Description                                    | Type of `$event`                     |
| ----------- | ---------------------------------------------- | ------------------------------------ |
| `selection` | triggered when a row is selected or unselected | `MthGridItem[]`                      |
| `error`     | triggered when there is an error with the API  | `{ title: string, message: string }` |

### Slots

| Name          | Description                                             | Data                          |
| ------------- | ------------------------------------------------------- | ----------------------------- |
| `gridActions` | buttons for each row to display in the `Actions` column | `{ itemAction: MthGridItem }` |

### Use example

This web component requires [Vuetify](https://vuetifyjs.com/en/).

```html
<MthNetworkGrid
  mode="remote"
  backendPath="http://localhost:1337"
  :filterBy="null"
  @selection="selection($event)"
>
  <template #gridActions="{ itemAction }">
    <v-btn @click="console.log(itemAction)">Info</v-btn>
  </template>
</MthNetworkGrid>

<script setup lang="ts">
import { MthNetworkGrid } from "@metabohub/mth-metexplore-biodata-grid";
</script>

<style scoped>
@import "@metabohub/mth-metexplore-biodata-grid/dist/style.css";
</style>
```

## MthBiodataGrid

### Props

| Name                        | Description                                                 | Type                  | Default     |
| --------------------------- | ----------------------------------------------------------- | --------------------- | ----------- |
| `mode`                      | remote : the data is fetched from the API with `backendPath`. local : the user provides the data with `data`. | `"local"\|"remote"`                                 | -          |
| `backendPath?`              | path to the api if mode is remote                           | `string`              | -           |
| `network?`                  | Id of the network if mode is remote                         | `number`              | -           |
| `data?`                     | the data to display if mode is local                        | `MthGridItem[]`       | -           |
| `biodata`                   | Type of biodata to display                                  | `Biodata`             | -           |
| `filterOnIds?`              | List of the ids to display, or null to display all the data | `Array<string>\|null` | `null`      |
| `mapping?`                  | the mappings with their list of mapped ids                  | `MappingColumns`      | `undefined` |
| `numberMappingPreSelected?` | the maximum number of mappings that will show in the table  | `number`              | 3           |
| `selectStrategy?`           |	the type of select for the rows : single or multiple        | `SelectStrategy`      | `"all"`     |

## Events

| Name        | Description                                    | Type of `$event`                     |
| ----------- | ---------------------------------------------- | ------------------------------------ |
| `selection` | triggered when a row is selected or unselected | `MthGridItem[]`                      |
| `error`     | triggered when there is an error with the API  | `{ title: string, message: string }` |

## Slots

| Name          | Description                                             | Data                          |
| ------------- | ------------------------------------------------------- | ----------------------------- |
| `gridActions` | buttons for each row to display in the `Actions` column | `{ itemAction: MthGridItem }` |

## Use example

This web component requires [Vuetify](https://vuetifyjs.com/en/).

```html
<MthBiodataGrid
  mode="remote"
  backendPath="http://localhost:1337"
  :network="207"
  :biodata="Biodata.METABOLITE"
  @selection="selection($event)"
>
  <template #gridActions="{ itemAction }">
    <v-btn @click="info(itemAction)">Info</v-btn>
  </template>
</MthBiodataGrid>

<script setup lang="ts">
import { MthMetaboliteGrid, Biodata } from "@metabohub/mth-metexplore-biodata-grid";
</script>

<style scoped>
@import "@metabohub/mth-metexplore-biodata-grid/dist/style.css";
</style>
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

Check the coverage of the unit tests :
```sh
npm run coverage
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### View documentation

```sh
npm run storybook
```

## CICD pipeline

### Tests

```yml
test:
  image: node:latest
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:unit
```

This runs the unit tests defined in `src/components/__tests__/`

### Deploy

```yml
.publish:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y git default-jre
    - npm install
    - npm run build
```

This builds the component as an npm package.
